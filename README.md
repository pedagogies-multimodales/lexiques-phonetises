Provenance des lexiques :


| Fichier                | Nb mots | Source                                                                                                                                                            |
|------------------------|---------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| chinese_mandarin.tsv   | 57419   | K3M                                                                                                                                                               |
| chinese_pinyin.tsv     | 57283   | K3M                                                                                                                                                               |
| english.tsv            | 113437  | [CMU Dictionary](http://www.speech.cs.cmu.edu/cgi-bin/cmudict) (Northern American English) 134,000 words, ARPAbet                                                                                                                                                    |
| britfone.main.3.0.1.csv| 113437  | [Britfone](https://github.com/JoseLlarena/Britfone) (British English) 16,000 words, IPA, MIT license                                                                                                                                                    |
| french.tsv             | 142691  | Lexique.org v381                                                                                                                                                  |
| italian.tsv            | 637     | [Fait maison par S. Daronnat et E. Melnikova](https://github.com/daronnat/phonochrome)                                                                            |
| japanese.tsv           | 207852  | K3M                                                                                                                                                               |
| korean.tsv             | 51229   | K3M                                                                                                                                                               |
| russian.tsv            | 405     | [Fait maison par S. Daronnat et E. Melnikova](https://github.com/daronnat/phonochrome)                                                                            |
| spanish.tsv            | 599     | [Fait maison par S. Daronnat et E. Melnikova](https://github.com/daronnat/phonochrome)                                                                            |
| taiwanese_mandarin.tsv | 12604   | K3M                                                                                                                                                               |
| taiwanese_pinyin.tsv   | 12604   | K3M                                                                                                                                                               |
| wenzhou.tsv            | 500     | HENGZHANG Shangfang 鄭張尚芳, 2008, Annales du dialecte de Wenzhou《溫州方言志 》 [Wenzhou Fangyan Zhi], Pékin : Zhonghua Book Company (中華書局 [Zhongua Shuju]) |

